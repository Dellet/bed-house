"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import webp from "gulp-webp";
import debug from "gulp-debug";
import browsersync from "browser-sync";

gulp.task("webp", () => {
	return gulp.src(paths.webp.src)
		.pipe(webp())
		.pipe(gulp.dest(paths.webp.dist))
		.pipe(debug({
			"title": "Images"
		}))
		.on("end", browsersync.reload);
});
