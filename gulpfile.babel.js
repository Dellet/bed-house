"use strict";

import gulp from "gulp";

const requireDir = require("require-dir");
const paths = {
	pug: {
		src: [
			"./src/pages/**/*.pug"
		],
		dist: "./dist/",
		watch: [
			"./src/blocks/**/*.pug",
			"./src/pages/**/*.pug",
			"./src/pug/**/*.pug"
		]
	},
	styles: {
		src: [
			"./src/styles/critical-styles.scss",
			"./src/styles/bundle.scss"
		],
		dist: "./dist/styles/",
		watch: [
			"./src/blocks/**/*.scss",
			"./src/styles/**/*.scss"
		]
	},
	scripts: {
		src: [
			"./src/js/critical-scripts.js",
			"./src/js/bundle.js",
		],
		dist: "./dist/js/",
		watch: [
			"./src/blocks/**/*.js",
			"./src/js/**/*.js"
		]
	},
	images: {
		src: [
			"./src/img/**/*.{jpg,jpeg,png,gif,tiff,svg}",
			"!./src/img/favicons/*.{jpg,jpeg,png,gif,tiff}"
		],
		dist: "./dist/img/",
		watch: "./src/img/**/*.{jpg,jpeg,png,gif,svg}"
	},
	webp: {
		src: [
			"./src/img/**/*.{jpg,jpeg,png,tiff}",
			"!./src/img/favicons/*.{jpg,jpeg,png,gif}",
			"!./src/img/preview-social-link/*.{jpg,jpeg,png,gif,tiff}"
		],
		dist: "./dist/img/",
		watch: [
			"./src/img/**/*.{jpg,jpeg,png,tiff}",
			"!./src/img/favicon.{jpg,jpeg,png,gif}",
			"!./src/img/preview-social-link/*.{jpg,jpeg,png,gif,tiff}"
		]
	},
	fonts: {
		src: "./src/fonts/**/*.{woff,woff2}",
		dist: "./dist/fonts/",
		watch: "./src/fonts/**/*.{woff,woff2}"
	}
};

requireDir("./gulp-tasks/");

export { paths };

export const development = gulp.series("clean",
	gulp.parallel(["styles", "scripts", "pug", "fonts"]),
	gulp.parallel("serve"));

export const prod = gulp.series("clean",
	gulp.series(["styles", "scripts", "pug", "inline-source", "images", "fonts"]));

export default development;
