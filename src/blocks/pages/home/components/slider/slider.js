// -- Helpers BEGIN
const documentReady = require("%helpers%/document-ready.js");
// -- Helpers END


// -- Libraries BEGIN
import Swiper from "swiper";
// -- Libraries END


documentReady(function() {
	const $slider = $(".slider .swiper-container");
	const $btnPrev = $(".slider .slider__btn_prev");
	const $btnNext = $(".slider .slider__btn_next");


	// -- Functions BEGIN
	const initSlider = () => {
	    const sliderOptions = {
			speed: 800,
			loop: false,
			slidesPerView: 1,
			navigation: {
				prevEl: $btnPrev,
				nextEl: $btnNext,
			},
			breakpoints: {
				1300: {

				},
			}
		};

		const initSwiper = new Swiper($slider, sliderOptions);
	};
	// -- Functions END


	if ($slider.length) initSlider();
});
