const ready = require("%helpers%/document-ready.js");

ready(function() {

	const $btnsShowPopups = $(".modal-btn");
	const $elementsHidePopups = $(".close-modal");
	const $allPopups = $(".popup");
	const $page = $("html, body");

	let $curPopup;
	let $curBtn;

	// -- Functions BEGIN
	const showPopup = () => {
		const key = $curBtn.attr("data-popup-key");
		$curPopup = $(`.popup[data-popup-key=${key}]`);

		if ( $curPopup.length ) {
			$allPopups.removeClass("popup_visible");
			$curPopup.addClass("popup_visible disabled");
			$page.addClass("page-fixed");

			setTimeout(function() {
				$curPopup.removeClass("disabled");
			}, 800); // -- After popups show animation show
		}
	};

	const hidePopups = () => {
		$allPopups.removeClass("popup_visible");
		$page.toggleClass("page-fixed");
	};
	// -- Functions END


	// -- Show popop BEGIN
	if ($btnsShowPopups.length && $allPopups.length) {
		$btnsShowPopups.on("click", function (e) {
			e.preventDefault();
			$curBtn = $(this);

			showPopup();
		});
	}
	// -- Show popop END


	// -- Hide popups BEGIN
	if ($elementsHidePopups.length && $allPopups.length) {
		$elementsHidePopups.on("click", function () {
			hidePopups();
		});
	}
	// -- Hide popups END

});