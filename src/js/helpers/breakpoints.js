const breakpoints = {
	xl: 1440,
	lg: 992,
	md: 768,
	sm: 576
};

module.exports = breakpoints;
