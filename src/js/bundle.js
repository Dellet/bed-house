import "%vendor%/libs/modernizr-custom.js";
import "%js%/common/lazyload.js";
import "%js%/common/replace-svg-to-inline.js";
import "%js%/common/tabs.js";
import "%js%/common/scroll-to-block";
import "%js%/common/cart";
import "%js%/common/form";

import "%blocks%/blocks.js";